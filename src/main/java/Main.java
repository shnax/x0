import game.WinPredicate;
import game.adapter.ConsolGameAdapter;
import game.adapter.GameAdapter;
import game.BaseStatableGame;

public class Main
{
  public static void main(String[] args)
  {
    GameAdapter game = new ConsolGameAdapter(new BaseStatableGame(new WinPredicate()));

    game.start();
  }
}
