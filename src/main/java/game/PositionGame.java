package game;

import game.cell.Cell;

public interface PositionGame extends Game
{
  Cell[][] getPosition();
}
