package game;

import java.util.function.Predicate;

import game.cell.Cell;

public class BaseStatableGame extends BaseGame implements PositionGame
{
  public BaseStatableGame(Predicate<Cell[][]> winCondition)
  {
    super(winCondition);
  }

  @Override
  public Cell[][] getPosition()
  {
    return this.places;
  }
}
