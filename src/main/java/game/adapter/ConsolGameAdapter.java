package game.adapter;

import java.util.Scanner;

import game.GameState;
import game.PositionGame;

public class ConsolGameAdapter implements GameAdapter
{
  private PositionGame game;

  public ConsolGameAdapter(PositionGame game)
  {
    this.game = game;
  }

  @Override
  public void start()
  {
    Scanner scanner = new Scanner(System.in);

    int x, y;
    int count = -1;

    do
    {
      do
      {
        printState();

        System.out.println("Enter X:");
        x = (scanner.nextInt());

        System.out.println("Enter Y:");
        y = (scanner.nextInt());
      }
      while (!game.makeСhoice(x, y));

      ++count;
    }
    while (game.getGameState() == GameState.IN_PROCESS);

    if (game.getGameState() == GameState.WIN)
    {
      System.out.println("Game over: " + (count % 2 + 1) + " player win");
    }
    else
    {
      System.out.println("Game over: friendship win");
    }
  }

  private void printState()
  {
    for (int i = 0; i < 3; ++i)
    {
      for (int j = 0; j < 3; ++j)
      {
        System.out.print(game.getPosition()[i][j].getName() + "|");
      }

      System.out.println();
    }
  }
}
