package game;

import java.util.function.Predicate;

import game.cell.Cell;
import game.cell.CellEnum;

public class WinPredicate implements Predicate<Cell[][]>
{
  @Override
  public boolean test(Cell[][] cells)
  {
    return checkDiagonals(cells) || checkRows(cells) || checkColumns(cells);
  }

  private boolean checkColumns(Cell[][] cells)
  {
    return checkColumn(cells, 0) || checkColumn(cells, 1) || checkColumn(cells, 2);
  }

  private boolean checkColumn(Cell[][] cells, int columnIndex)
  {
    Cell first = cells[0][columnIndex];

    if(first == CellEnum.Blank)
    {
      return false;
    }
    
    return first == cells[1][columnIndex] && first == cells[2][columnIndex];
  }

  private boolean checkRows(Cell[][] cells)
  {
    return checkRow(cells[0]) || checkRow(cells[1]) || checkRow(cells[2]);
  }

  private boolean checkRow(Cell[] cell)
  {
    Cell first = cell[0];
    
    if(first == CellEnum.Blank)
    {
      return false;
    }
    
    return first == cell[1] && first == cell[2];
  }

  private boolean checkDiagonals(Cell[][] cells)
  {
    return checkMainDiagonal(cells) || checkSecondDiagonal(cells);
  }

  private boolean checkSecondDiagonal(Cell[][] cells)
  {
    Cell first = cells[0][2];

    if(first == CellEnum.Blank)
    {
      return false;
    }
    
    return first == cells[1][1] && first == cells[2][0];
  }

  private boolean checkMainDiagonal(Cell[][] cells)
  {
    Cell first = cells[0][0];

    if(first == CellEnum.Blank)
    {
      return false;
    }
    
    return first == cells[1][1] && first == cells[2][2];
  }
}
