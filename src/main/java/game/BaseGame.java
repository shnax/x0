package game;

import java.util.Arrays;
import java.util.function.Predicate;

import game.cell.Cell;
import game.cell.CellEnum;

public class BaseGame implements Game
{
  protected Cell places[][];
  private Predicate<Cell[][]> winCondition;
  private int count;

  public BaseGame(Predicate<Cell[][]> winCondition)
  {
    this.winCondition = winCondition;
    init();
  }
  private void init()
  {
    count = 0;
    places = new Cell[3][3];
    for (int i = 0; i < 3; ++i)
    {
      for (int j = 0; j < 3; ++j)
      {
        places[i][j] = CellEnum.Blank;
      }
    }
  }

  public boolean makeСhoice(int x, int y)
  {
    if (x > 2 || x < 0 || y > 2 || y < 0 || places[x][y] != CellEnum.Blank)
    {
      return false;
    }

    places[x][y] = count % 2 == 0 ? CellEnum.X : CellEnum.O;
    ++count;

    return true;
  }

  @Override
  public GameState getGameState()
  {
    if(winCondition.test(places))
    {
      return GameState.WIN;
    }
    
    if(allAreNotBlank(places))
    {
      return GameState.DRAW;
    }
    
    return GameState.IN_PROCESS;
  }

  private boolean allAreNotBlank(Cell[][] places)
  {
    for (int i = 0; i < 3; ++i)
    {
      for (int j = 0; j < 3; ++j)
      {
        if(places[i][j] == CellEnum.Blank)
        {
          return false;
        }
      }
    }
    
    return true;
  }
}
