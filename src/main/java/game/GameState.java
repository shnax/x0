package game;

public enum GameState
{
  IN_PROCESS, WIN, DRAW
}
