package game.cell;

public enum CellEnum implements Cell
{
  X("X"), O("O"), Blank("_");
  
  private String name;

  CellEnum(String name)
  {
    this.name = name;
  }

  @Override
  public String getName()
  {
    return name;
  }
}
